# README #
## Overview ##
Use the following native Azure PaaS services to build a simple ELT data pipeline: Azure Data Factory, Blob Storage, and Azure Synapse SQL (Dedicated SQL Pool i.e. Azure Data Warehouse).   The end goal is to demonstrate a basic understanding of these core technologies in Azure.

## Repository Contents ##
+ Word doc with exercise details and steps: Azure Data Pipeline.docx
+ Powershell script to create needed Azure Components: CreateEnvironment.ps1
+ SQL script to create the final SQL schema needed for the exercise: ExerciseSQLSchema.sql
+ Folder containing a sample solution to the exercise, including ADF and SQL pieces.

#### Topics covered ####
+ Orchestrate the pipeline and control data ingestion using ADF
+ Including activities, source/sink, expressions, datasets, Integration Runtimes
+ Using blob storage as a raw data store
+ Using Azure Synapse SQL  to read from blob storage in order to process data efficiently into a tabular model
+ Ingestion into Azure Synapse SQL using virtual tables

## Pre-requisites ##
1. Azure subscription (Setup a free account with MS if Daugherty is not available)
2. Tool to connect to Azure SQL Server: Azure Data Studio recommended
3. We'll need the following Azure components: 
	+ **Note: Instructions provided below to create these manually or use the included script.  If doing manually please use the naming conventions shown.**
	+ **NOTE: The script will use the Central US region.**
	+ Resource group to contain our resources 
	+ Storage Account
	+ Azure Data Factory workspace
	+ Azure Dedicated SQL Pool instance (formerly SQL DW)

## Environment Setup ##

#### Setup Azure Environment Using Portal ####
1. Resource group to contain our resources 
	+ Name: daughertyADFpoc<initials>-rg
2. Storage Account
	+ [Documentation](https://docs.microsoft.com/en-us/azure/storage/common/storage-account-create?tabs=azure-portal)
	+ Name : dbsadfpoc<initials>store
	+ StandardV2, LRS, Hot tier
3. Azure Data Factory workspace
	+ [Documentation](https://docs.microsoft.com/en-us/azure/data-factory/quickstart-create-data-factory-portal)
	+ Name: dbsADFworkspace-<initials>
4. Azure Dedicated SQL Pool instance (formerly SQL DW)
	+ [Documentation](https://docs.microsoft.com/en-us/azure/synapse-analytics/sql-data-warehouse/create-data-warehouse-portal)
	+ ServerName: dbsadfpocsql-<initials>
	+ AdminUser:sadaugherty
	+ Pw: DBSadfExercise2022!
	+ DB Name: APIData
	+ Set compute to DW100c
	+ Firewalls and networks
		+ Add Client IP (your machine's public IP)

#### Setup Azure Environment using Script ####
1. Go to portal.azure.com and sign in
2. Open the cloud shell by selecting the icon at the top of the page
3. Select Powershell for your configuration, follow instructions to configure cloud shell.  It will create a storage account for itself.
4. Find the included CreateEnvironment.ps1 script
	+ **NOTE: The script is using a mix of powershell and the Azure CLI to create the resources needed for this exercise.**
5. Upload the script to the cloud shell
6. From the prompt, run the script
7. Follow the prompts that the script provides and let it run to completion
8. Verify your resources in the Azure Portal
9. Test your connection to the SQL Server using the provided admin user and pw
	+ **NOTE: By default, the database will be operational once created. If you won't be using it for a while you should pause the database to avoid incurring an hourly compute cost. This can be done from the portal.**
	
#### Setup SQL Environment ####
1. Using SSMS or Azure Data Studio, connect to your SQL server
2. Run the included ExerciseSQLSchema.sql script to create the following
	+ staging and reporting schemas
	+ reporting tables as the final structure to process the data into
	+ staging.cp_RunTest stored procedure to run as the validation test

### Once the environment is setup, Follow the word doc to go through the exercise.

### Questions or issues about this exercise? ###
* Matt Fischer - matt.fischer@daugherty.com


