create procedure staging.cp_ProcessData
AS
BEGIN
    create table staging.RawData 
    WITH
    (
    DISTRIBUTION = ROUND_ROBIN
    ,CLUSTERED COLUMNSTORE INDEX
    )
    AS
    select cast(JSON_VALUE(JsonData, '$.id') as int) as id
        , JSON_VALUE(JsonData, '$.uid') as uid
        , JSON_VALUE(JsonData, '$.blend_name') as blendName
        , JSON_VALUE(JsonData, '$.origin') as origin 
        , JSON_VALUE(JsonData, '$.variety') as variety
        , JSON_VALUE(JsonData, '$.notes') as notes
        , JSON_VALUE(JsonData, '$.intensifier') as intensifier   
    from staging.BlobStorage
    ;

    insert into reporting.variety(VarietyDesc)
    select distinct variety
    from staging.RawData;

    insert into reporting.origin(OriginDesc)
    select distinct origin
    from staging.RawData;

    insert into reporting.intensifier(IntensifierDesc)
    select distinct intensifier
    from staging.RawData;

    insert into reporting.coffee(CoffeeId, BlendName, Notes, VarietyId, IntensifierId, OriginId)
    select r.uid
        , r.blendName
        , r.Notes
        , v.VarietyId
        , i.IntensifierId
        , o.OriginId
    from staging.RawData r
    inner join reporting.variety v on v.VarietyDesc = r.variety
    inner join reporting.origin o on o.OriginDesc = r.origin
    inner join reporting.intensifier i on i.IntensifierDesc = r.intensifier
    ;

    insert into reporting.VarietyTotal(VarietyId, TotalRecords)
    select VarietyId, count(1)
    from reporting.Coffee
    group by VarietyId

    insert into reporting.IntensifierTotal(IntensifierId, TotalRecords)
    select IntensifierId, count(1)
    from reporting.Coffee
    group by IntensifierId

    insert into reporting.OriginTotal(OriginId, TotalRecords)
    select OriginId, count(1)
    from reporting.Coffee
    group by OriginId
END;

exec staging.cp_RunTest

--Cleanup
drop table staging.RawData;
truncate table reporting.variety;
truncate table reporting.intensifier;
truncate table reporting.origin;
truncate table reporting.coffee;
truncate table reporting.VarietyTotal;
truncate table reporting.IntensifierTotal;
truncate table reporting.OriginTotal;