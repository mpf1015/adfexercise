--Create final reporting schema
create schema reporting AUTHORIZATION dbo;
go
create schema staging AUTHORIZATION dbo;

GO
--Create External Resources to read from blob storage
CREATE MASTER KEY
GO
CREATE DATABASE SCOPED CREDENTIAL BlobCred
WITH IDENTITY = 'mpfadfpocstore'
    , SECRET = 'KGRMBhx6j9t4lZR40cRTTLdcBcojWYKPE0VAj/HnBFpc2TuaHaKSsJ5qlwER50wPKChxvODXbmPlKsdhwDUl8Q==';

create external data source BlobStorage
with (LOCATION = 'wasbs://adfpoc@mpfadfpocstore.blob.core.windows.net'
    ,Credential = BlobCred
    , type = HADOOP);

CREATE EXTERNAL FILE FORMAT JsonFiles  
WITH (  
    FORMAT_TYPE = DELIMITEDTEXT
    , FORMAT_OPTIONS (  
        FIELD_TERMINATOR = '|'
        ,Encoding = 'UTF8')       
    ); 

CREATE EXTERNAL TABLE staging.BlobStorage
    (JsonData nvarchar(max) )
    WITH (
        LOCATION = '/apilanding/',
        DATA_SOURCE = BlobStorage,
        FILE_FORMAT = JsonFiles);