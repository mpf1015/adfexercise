/*
Modification could cause false test results when finished
*/
create schema reporting AUTHORIZATION dbo;
GO
create schema staging AUTHORIZATION dbo;
GO
create table reporting.variety 
(
    VarietyId int IDENTITY(1,1)
    , VarietyDesc varchar(100)
);
GO
create table reporting.intensifier
(
    IntensifierId int IDENTITY(1,1)
    , IntensifierDesc varchar(50)
);
GO
create table reporting.origin
(
    OriginId int IDENTITY(1,1)
    , OriginDesc varchar(50)
);
GO
create table reporting.coffee
(
    CoffeeId varchar(40)
    , BlendName varchar(200)
    , Notes varchar(1000)
    , VarietyId INT
    , IntensifierId INT
    , OriginId INT    
);
GO
create table reporting.VarietyTotal
(
    VarietyId INT
    , TotalRecords INT
);
GO
create table reporting.OriginTotal
(
    OriginId INT
    , TotalRecords INT
);
GO
create table reporting.IntensifierTotal
(
    IntensifierId INT
    , TotalRecords INT
);
GO
--Test Proc
create proc staging.cp_RunTest
AS
BEGIN
    DECLARE @TestsPassed TINYINT = 0


    DECLARE @Storage int;
    select @Storage = count(JSON_VALUE(JsonData, '$.uid'))
    from staging.BlobStorage
    IF @Storage > 1
        BEGIN
            SET @TestsPassed +=1
            PRINT 'Test Passed - Storage accessible'
        END
    ELSE
        BEGIN
            PRINT 'Test Failed - Storage inaccessible'
        END

    DECLARE @TotalCount int
    SELECT @TotalCount = count(1) from reporting.coffee
    IF @TotalCount = 500
        BEGIN
            SET @TestsPassed +=1
            PRINT 'Test Passed - 500 Total Records in coffee'
        END
    ELSE
        BEGIN
            PRINT 'Test Failed - Total records in coffee not correct <> 500'
        END

    DECLARE @VarietyCount int, @RawVariety int
    select @RawVariety = count(distinct JSON_VALUE(JsonData, '$.variety'))
    from staging.BlobStorage
    SELECT @VarietyCount = count(1) from reporting.VarietyTotal
    IF @VarietyCount = @RawVariety
        BEGIN
            SET @TestsPassed +=1
            PRINT 'Test Passed - VarietyTotal matches raw data'
        END
    ELSE
        BEGIN
            PRINT 'Test Failed - VarietyTotal('+convert(varchar,@VarietyCount) +') does not match raw data ('+convert(varchar, @RawVariety)+')'
        END

    DECLARE @OriginCount int, @RawOrigin int
    select @RawOrigin = count(distinct JSON_VALUE(JsonData, '$.origin'))
    from staging.BlobStorage
    SELECT @OriginCount = count(1) from reporting.OriginTotal
    IF @OriginCount = @RawOrigin
        BEGIN
            SET @TestsPassed +=1
            PRINT 'Test Passed - OriginTotal matches raw data'
        END
    ELSE
        BEGIN
            PRINT 'Test Failed - OriginTotal('+convert(varchar,@OriginCount) +') does not match raw data ('+convert(varchar, @RawOrigin)+')'
        END

    DECLARE @IntensifierCount int, @RawIntensifier int
    select @RawIntensifier = count(distinct JSON_VALUE(JsonData, '$.intensifier'))
    from staging.BlobStorage
    SELECT @IntensifierCount = count(1) from reporting.IntensifierTotal
    IF @IntensifierCount = @RawIntensifier
        BEGIN
            SET @TestsPassed +=1
            PRINT 'Test Passed - IntensifierTotal matches raw data'
        END
    ELSE
        BEGIN
            PRINT 'Test Failed - IntensifierTotal('+convert(varchar,@IntensifierCount) +') does not match raw data ('+convert(varchar, @RawIntensifier)+')'
        END
    

    IF @TestsPassed = 5
    BEGIN
        PRINT 'All Tests have passed'
    END

    SELECT @TestsPassed AS TestsPassed
END;