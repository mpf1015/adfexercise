#az login
$initials = Read-Host "Please enter initials.  To be used to dynamically name resources"
$clientIP = Read-Host "Please enter your Public IP of the machine you are using for this (can go to https://ifconfig.me/ip)
This is used to configure access to the SQL Server from this machine (ex 0.0.0.0)"

#create resource group
$rgName = 'daughertyADFpoc'+$initials+'-rg'
$rgExist = az group exists -n $rgName
if(-Not $rgExist ){
    az group create -l centralUS -n $rgName
}
else {
    Write-Host "Resource group already exists"
}

#create storage account
$storageName = 'dbsadfpoc'+$initials+'store'
[string]$stgExist = az storage account check-name --name $storageName
$stgObj = ConvertFrom-Json($stgExist)
if ($stgObj.nameAvailable -eq "False"){
    az storage account create --name $storageName -l centralUS --resource-group $rgName --sku Standard_LRS --kind StorageV2 --public-network-access Enabled
}
else {
    $stgObj.message
}

#create data factory workspace
$adfName = 'dbsADFworkspace-'+$initials
az datafactory create --location "Central US" --name $adfName --resource-group $rgName


#create synapse sql pool
$sqlServerName = 'dbsadfpocsql-'+$initials
az sql server create --resource-group $rgName --name $sqlServerName --admin-user sadaugherty --admin-password DBSadfExercise2022! --minimal-tls-version 1.2
az sql server firewall-rule create -g $rgName -s $sqlServerName -n AzServices --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0
az sql server firewall-rule create --resource-group $rgName --name AllowSome --server $sqlServerName --start-ip-address $clientIP --end-ip-address $clientIP
az sql dw create --name APIData --resource-group $rgName --server $sqlServerName --service-objective DW100c --zone-redundant false --collation SQL_Latin1_General_CP1_CI_AS --bsr Local

Write-Host -ForegroundColor Yellow "Environment script finished, check for any errors"